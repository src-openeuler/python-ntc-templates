%global _empty_manifest_terminate_build 0
Name:           python-ntc-templates
Version:        4.4.0
Release:        1
Summary:        TextFSM Templates for Network Devices, and Python wrapper for TextFSM's CliTable.
License:        Apache-2.0
URL:            https://github.com/networktocode/ntc-templates
Source0:        https://files.pythonhosted.org/packages/fe/a6/903ec6a1de234dd3dd4a448ec91e038a96eb1036f8f8da807c2614d5b0d0/ntc_templates-4.4.0.tar.gz
BuildArch:      noarch
%description
TextFSM Templates for Network Devices, and Python wrapper for TextFSM's CliTable.

%package -n python3-ntc-templates
Summary:        TextFSM Templates for Network Devices, and Python wrapper for TextFSM's CliTable.
Provides:       python-ntc-templates
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
# General requires
BuildRequires:  python3-textfsm
# General requires
Requires:       python3-textfsm
%description -n python3-ntc-templates
TextFSM Templates for Network Devices, and Python wrapper for TextFSM's CliTable.

%package help
Summary:        TextFSM Templates for Network Devices, and Python wrapper for TextFSM's CliTable.
Provides:       python3-ntc-templates-doc
%description help
TextFSM Templates for Network Devices, and Python wrapper for TextFSM's CliTable.

%prep
%autosetup -n ntc_templates-%{version}

%build
%py3_build

%install
%py3_install

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%check
%{__python3} setup.py test

%files -n python3-ntc-templates -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Tue Apr 02 2024 wangqiang <wangqiang1@kylinos.cn> - 4.4.0-1
- Update package to version 4.4.0

* Fri Feb 10 2023 wubijie <wubijie@kylinos.cn> - 3.2.0-1
- Update package to version 3.2.0

* Mon Nov 7 2022 hkgy <kaguyahatu@outlook.com> - 3.1.0-1
- Upgrade package python3-ntc-templates to version 3.1.0

* Wed Jun 01 2022 OpenStack_SIG <openstack@openeuler.org> - 3.0.0-1
- Upgrade package python3-ntc-templates to version 3.0.0

* Mon Jul 26 2021 OpenStack_SIG <openstack@openeuler.org> - 1.7.0-1
- Package Spec generate
